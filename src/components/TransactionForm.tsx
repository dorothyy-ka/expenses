import React, { Component } from "react";
import PropTypes from "prop-types";

/** @jsx jsx */
import styled from "@emotion/styled";
import { css, jsx } from "@emotion/core";

import FormItem from './FormItem';

export default class TransactionForm extends Component {
  handleChange = event => {
    event.preventDefault();
    this.props.onChange(event);
  };

  handleSubmit = event => {
    event.preventDefault();
    this.props.onSubmit();
  };

  renderFormErrors() {
    const { formErrors } = this.props;

    const Errors = styled.div`
      color: #ff0000;
    `;

    return (
      <Errors>
        {Object.keys(formErrors).map((fieldName, i) =>
          formErrors[fieldName].length > 0 ? (
            <p key={i}>{formErrors[fieldName]}</p>
          ) : (
            ""
          )
        )}
      </Errors>
    );
  }

  render() {
    const formStyles = css`
      margin-top: 40px;
      font-size: 18px;
      display: flex;
      flex-wrap: wrap;
    `;

    const Submit = styled.input`
      width: 100px;
      border-radius: 5px;
      background-color: #e6e6ff;
      border: 1px solid #000;
      transition: 0.3s ease-in-out background-color;
      cursor: pointer;
      :hover,
      :focus,
      :active {
        background-color: #ccccff;
      }

      :disabled {
        background-color: #d3d3d3;
        cursor: default;
      }
    `;

    return (
      <React.Fragment>
        <form onSubmit={this.handleSubmit} css={formStyles}>
          <FormItem
            label="Title of transaction"
            name="title"
            type="text"
            value={this.props.newTransaction.title}
            onChange={this.handleChange}
          />
          <FormItem
            label="Amount (in PLN)"
            name="amountPLN"
            type="number"
            value={this.props.newTransaction.amountPLN}
            onChange={this.handleChange}
          />
          <Submit
            type="submit"
            value="Add"
            disabled={this.props.disabledSubmit}
          />
        </form>
        {this.renderFormErrors()}
      </React.Fragment>
    );
  }
}

TransactionForm.propTypes = {
  onChange: PropTypes.func,
  onSubmit: PropTypes.func,
  formErrors: PropTypes.object,
  newTransaction: PropTypes.object,
  disabledSubmit: PropTypes.bool
};
