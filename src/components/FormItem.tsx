import PropTypes from "prop-types";

/** @jsx jsx */
import { css, jsx } from "@emotion/core";

interface FormItemProps {
  label: string;
  name: string;
  type: string;
  value: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const FormItem = ({ label, name, type, value, onChange }: FormItemProps) => {
  const formItemStyles = css`
    padding-right: 45px;
    width: calc(100% - 180px);
    font-size: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    :first-of-type {
      margin-bottom: 15px;
    }

    span {
      width: 300px;
      white-space: nowrap;
    }

    input {
      padding-left: 15px;
      width: 100%;
      line-height: 30px;
    }
  `;

  return (
    <label css={formItemStyles}>
      <span>{label}</span>
      <input
        key={name}
        name={name}
        type={type}
        value={value}
        onChange={onChange}
      />
    </label>
  );
};

FormItem.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string,
  onChange: PropTypes.func
};

export default FormItem;
