/** @jsx jsx */
import { css, jsx } from "@emotion/core";

import Table from "@simple-gui/react-simple-table";
import PropTypes from "prop-types";

const TransactionListTable = ({ transactions, onDelete }) => {
  const tableStyles = css`
    margin-top: 40px;
    width: 100%;
    border-collapse: collapse;

    &,
    th,
    td {
      border: 1px solid #000;
    }

    th,
    td {
      padding: 0 15px;
    }

    th {
      height: 50px;
      font-weight: 400;
      font-size: 22px;
      text-align: left;
      background-color: #cccccc;
    }

    tr {
      &:nth-of-type(even) {
        background-color: #e0e0eb;
      }
    }

    td {
      height: 45px;
      font-size: 18px;

      button {
        font-size: inherit;
        border: none;
        background: none;
        cursor: pointer;
        outline: none;

        &:hover {
          color: #6666ff;
        }
      }
    }
  `;

  if (!transactions) {
    return null;
  }

  return (
    <Table
      css={tableStyles}
      data={transactions}
      columns={[
        {
          title: "Title",
          field: "title"
        },
        {
          title: "Amount(PLN)",
          field: "amountPLN"
        },
        {
          title: "Amount(EUR)",
          field: "amountEUR"
        },
        {
          title: "Options",
          // eslint-disable-next-line react/display-name
          component: t => <button onClick={() => onDelete(t.id)}>Delete</button>
        }
      ]}
    />
  );
};

TransactionListTable.propTypes = {
  transactions: PropTypes.array,
  onDelete: PropTypes.func
};

export default TransactionListTable;
