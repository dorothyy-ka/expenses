import PropTypes from "prop-types";

/** @jsx jsx */
import { jsx, css } from "@emotion/core";
import styled from "@emotion/styled";

import TransactionForm from "./TransactionForm";
import TransactionListTable from "./TransactionListTable";

const TransactionListWrapper = props => {
  const ContainerStyles = css`
    margin: 30px auto;
    padding: 10px;
    width: 800px;
    max-width: 100%;
  `;

  const Heading = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;
  `;

  const Title = styled.h1`
    margin: 0;
    font-weight: 500;
  `;

  const ExchangeRate = styled.span`
    font-size: 18px;
  `;

  const Sum = styled.div`
    margin-top: 50px;
    font-size: 22px;
  `;

  return (
    <div css={ContainerStyles}>
      <Heading>
        <Title>List of expenses</Title>
        <ExchangeRate>1EUR = 4.382PLN</ExchangeRate>
      </Heading>
      <TransactionForm {...props} />
      <TransactionListTable
        transactions={props.transactions}
        onDelete={props.onDelete}
      />
      <Sum>
        <span>
          Sum: {props.sumPLN}PLN ({props.sumEUR} EUR)
        </span>
      </Sum>
    </div>
  );
};

TransactionListWrapper.propTypes = {
  transactions: PropTypes.array,
  sumPLN: PropTypes.number,
  sumEUR: PropTypes.number,
  onDelete: PropTypes.func
};

export default TransactionListWrapper;
