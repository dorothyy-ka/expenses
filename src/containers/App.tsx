import React, { Component } from "react";
import { Provider } from "mobx-react";
import TransactionListWrapper from "./TransactionListWrapper";
import TransactionListStore from "../models/TransactionListStore";

export default class App extends Component {
  constructor() {
    super();
    this.store = new TransactionListStore();
  }

  render() {
    return (
      <Provider transactionListStore={this.store}>
        <TransactionListWrapper />
      </Provider>
    );
  }
}
