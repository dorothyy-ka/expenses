import React, { Component } from "react";
import PropTypes from "prop-types";
import { decorate, observable, action } from "mobx";
import { observer, inject } from "mobx-react";

import TransactionListWrapper from "../components/TransactionListWrapper";
import { selectTransactionListStore } from "../selectors/transactionListSelector";

const initCurrentState = {
  title: "",
  amountPLN: "",
  formErrors: {
    title: "",
    amountPLN: ""
  },
  titleValid: false,
  amountPLNValid: false
};

class UnwrappedTransactionListContainer extends Component {
  currentState = initCurrentState;

  render() {
    const { transactionListStore } = this.props;
    const newTransaction = {
      title: this.currentState.title,
      amountPLN: this.currentState.amountPLN
    };

    const formValid =
      this.currentState.titleValid && this.currentState.amountPLNValid;

    const handleChange = e => this.handleChange(e);
    const handleSubmit = () => this.handleSubmit();
    const handleDelete = id => this.handleDelete(id);

    return (
      <TransactionListWrapper
        transactions={transactionListStore.transactions}
        disabledSubmit={!formValid}
        formErrors={this.currentState.formErrors}
        onChange={handleChange}
        onSubmit={handleSubmit}
        newTransaction={newTransaction}
        onDelete={handleDelete}
        sumPLN={transactionListStore.sumPLN}
        sumEUR={transactionListStore.sumEUR}
      />
    );
  }

  handleChange = event => {
    const { name: fieldName, value } = event.target;

    let fieldValidationErrors = this.currentState.formErrors;
    let titleValid = this.currentState.titleValid;
    let amountPLNValid = this.currentState.amountPLNValid;

    switch (fieldName) {
      case "title":
        titleValid = value.length >= 5;
        fieldValidationErrors.title = titleValid
          ? ""
          : "Title should have at least 5 characters";
        break;
      case "amountPLN":
        amountPLNValid =
          value % 1 !== 0 ? value.toString().split(".")[1].length <= 2 : true;
        fieldValidationErrors.amountPLN = amountPLNValid
          ? ""
          : "Amount should accept at most two digits after the decimal point";
        break;
      default:
        break;
    }

    this.currentState = {
      ...this.currentState,
      [fieldName]: value,
      formErrors: fieldValidationErrors,
      titleValid,
      amountPLNValid
    };
  };

  handleSubmit() {
    const { title, amountPLN } = this.currentState;
    this.props.transactionListStore.addTransaction(title, amountPLN);
    this.currentState = initCurrentState;
  }

  handleDelete(id) {
    this.props.transactionListStore.removeTransaction(id);
  }
}

UnwrappedTransactionListContainer.propTypes = {
  transactionListStore: PropTypes.object
};

decorate(UnwrappedTransactionListContainer, {
  currentState: observable,
  handleChange: action,
  handleSubmit: action,
  handleOnDelete: action
});

export const TransactionListContainer = inject(selectTransactionListStore)(
  observer(UnwrappedTransactionListContainer)
);

TransactionListContainer.wrappedComponent.propTypes = {
  transactionListStore: PropTypes.object.isRequired
};

export default TransactionListContainer;
