import { decorate, observable } from "mobx";
import uuid from "uuid";

export default class TransactionItem {
  id = uuid();

  constructor(title, amountPLN) {
    this.title = title;
    this.amountPLN = amountPLN;
    this.amountEUR = Math.round((amountPLN / 4.382) * 100) / 100;
  }
}

decorate(TransactionItem, {
  title: observable,
  amountPLN: observable,
  amountEUR: observable
});
