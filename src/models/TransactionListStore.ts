import { decorate, observable, computed, action } from "mobx";
import TransactionItem from "./TransactionItem";

export default class TransactionListStore {
  transactionList = [
    new TransactionItem("New book about Rust", 100),
    new TransactionItem("Snaks for a football match", 20),
    new TransactionItem("Bus ticket", 2.55)
  ];

  get transactions() {
    return this.transactionList;
  }

  get sumPLN() {
    return this.transactionList.reduce(
      (acc, { amountPLN }) => acc + parseFloat(amountPLN),
      0
    );
  }

  get sumEUR() {
    const sum = this.transactionList.reduce(
      (acc, { amountEUR }) => acc + amountEUR,
      0
    );

    return Math.round(sum * 100) / 100;
  }

  addTransaction(title, amountPLN) {
    this.transactionList.push(new TransactionItem(title, amountPLN));
  }

  removeTransaction(id) {
    this.transactionList = this.transactionList.filter(
      transaction => transaction.id !== id
    );
  }
}

decorate(TransactionListStore, {
  transactionList: observable,
  sumPLN: computed,
  sumEUR: computed,
  addTransaction: action,
  removeTransaction: action
});
